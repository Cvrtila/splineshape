# SplineShape

A DomainShape that can reconstruct a surface from a given discretization with Catmull-Rom splines.

A typical use of this would be as follows. Suppose we are given a collection of points `Range<Vec2d> points` on the plane. First we conctruct a new `SplineShape` by calling

```c++
SplineShape sshape(points);
```

Suppose we know that the point `(0, 0)` is in the interior of the shape. We need to then call

```c++
sshape.setInterior(Vec2d(0, 0));
```

or in this case just

```c++
sshape.setInterior();
```

since the default value for this method is `Vec2d(0, 0)`.

To get a discretization of the boundary with spacing `double h` (or `function<double(double)>`), simply call

```c++
DomainDiscretization<Vec2d> dd = sshape.discretizeBoundaryWithDensity(h, type);
```

where `type` is a negative integer.

To test wether a point `Vec2d pt` is contained in the shape, call

```c++
sshape.contains(pt);
```

Note that we need to let the algorithm know what's the interior and what's the exterior using `setInterior`.

The constructor for `SplineShape` also accepts some optional parameters:
- a `function<scalar_t(scalar_t)> rbf_kernel` that is used to determine the interior/exterior of the shape. The default value is `pow(t, 3)`
- a `double alpha` used to determine the kind of Catmull-Rom spline used - see [this page](https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline)
- an `int k` - the number of neighbours used in (local) function fitting
- an `int seed` passed to `gsf`.
