clear;

run '../data/starting.m' % start
run '../data/added.m' % filled
run '../data/interior.m' % inpts
%run '../data/colors.m' % col
%run '../data/normals.m' % nrmls

%nrmls = -nrmls;
%nrmls = [-nrmls(2, :); nrmls(1, :)];
%nrmls = [nrmls(:, 2:end), nrmls(:, 1)];

colnorms = @(X) sqrt(sum(X.^2, 1));

inds = [1:size(start, 2); atan2(start(2, :), start(1, :))]';
inds = sortrows(inds, 2);
inds = inds(:, 1);

scatter(start(1, :), start(2, :));
axis equal;
hold on;
scatter(filled(1, :), filled(2, :), 'x');
cc = [0.4, 0.4, 0.4];
scatter(inpts(1, :), inpts(2, :), 1, cc)
quiver(filled(1, :), filled(2, :), nrmls(1, :), nrmls(2, :), 1);
hold off;

difs = colnorms(filled(:, 2:end) - filled(:, 1:end-1));
