#include <iostream>
#include <medusa/Medusa.hpp>
#include <medusa/bits/io/CSV_Eigen.hpp>
#include <random>

#include "SplineShape.hpp"

using namespace mm;

int main() {
    //// First we load parameters from the XML file in ccr_splines/data/
    XML param_doc("../data/params.xml");
    int n = param_doc.get<int>("start_point_gen.n");
    auto m = param_doc.get<int>("plot.n"); // The number of points for the mesh on which we test.

    auto h = param_doc.get<double>("gsf_params.h"); // Requested distance between pts for GSF
    auto seed = param_doc.get<int>("gsf_params.seed"); // The seed for GSF
    auto reps = param_doc.get<int>("plot.reps");
    int k = 10;

    double rand_range = 0.03;
    std::default_random_engine gen;
    std::uniform_real_distribution<double> dist(-rand_range, rand_range);
    Range<Vec2d> points(n);
    for (int i = 0; i < n; ++i) {
        double phi = ((double) i)/n * 2*PI;
        double nudge = dist(gen);
        //phi += nudge;
        points[i] = Vec2d(std::cos(phi), std::sin(phi));
    }

    for (int i = 0; i < reps; i++) {
        matlab_dump(points, "starting.m", "start");
        SplineShape<Vec2d> test_shape(points, seed, true, k);
        test_shape.setInterior(Vec2d(0, 0));
        DomainDiscretization<Vec2d> dd = test_shape.discretizeBoundaryWithDensity(3*h/2, h/2, -1);
        points = dd.positions();
    }
    matlab_dump(points, "added.m", "filled");

    SplineShape<Vec2d> shape(points, seed, true, k);
    shape.setInterior({0, 0});

    std::cout << "Testing interior" << std::endl;

    Range<Vec2d> int_pts;
    int_pts.reserve(m*m);
    Vec2d mins = {-1.1, -1.1};
    Vec2d maxs = {1.1, 1.1};
    double hx = (maxs(0) - mins(0))/m;
    double hy = (maxs(1) - mins(1))/m;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < m; ++j) {
            Vec2d pt = Vec2d(mins(0) + i*hx, mins(1) + j*hy);
            if (shape.contains(pt)) {
                int_pts.push_back(pt);
            }
        }
    }

    matlab_dump(int_pts, "interior.m", "in");

    return 0;
}
